package net.cubekrowd.armorstandcopy;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public final class LoadCommand implements Subcommand {
    private final ArmorStandCopyPlugin plugin;

    public LoadCommand(ArmorStandCopyPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "load";
    }

    @Override
    public String getUsage() {
        return "<file>";
    }

    @Override
    public String getDescription() {
        return "Loads the armor stands from the file and spawns them relative to you.";
    }

    private Vector decodeVector(ConfigurationSection section, String key) {
        var s = section.getConfigurationSection(key);
        return new Vector(s.getDouble("X"), s.getDouble("Y"), s.getDouble("Z"));
    }

    private EulerAngle decodeEulerAngle(ConfigurationSection section, String key) {
        var s = section.getConfigurationSection(key);
        return new EulerAngle(s.getDouble("X"), s.getDouble("Y"), s.getDouble("Z"));
    }

    private ArmorStand decodeAndSpawnArmorStand(ConfigurationSection section, String key,
            Location relativeTo) {
        var s = section.getConfigurationSection(key);
        // @TODO This try-catch is rather dirty (or is it?!) and should probably
        // be replaced by something cleaner.
        try {
            var loc = relativeTo.clone().add(decodeVector(s, "location"));
            loc.setDirection(decodeVector(s, "rotation"));
            // Initialise the armour stand before it is added to a world, so you
            // don't see the armour (and other stuff) pop on after a little
            // while. Note that any exceptions simply fall through to us.
            return relativeTo.getWorld().spawn(loc, ArmorStand.class, as -> {
                as.setGravity(s.getBoolean("gravity"));
                as.setInvulnerable(s.getBoolean("invuln"));
                as.setBasePlate(s.getBoolean("baseplate"));
                as.setArms(s.getBoolean("usearms"));
                as.setSmall(s.getBoolean("small"));
                as.setVisible(s.getBoolean("vis"));
                if (s.contains("display_name")) {
                    as.setCustomName(s.getString("display_name"));
                    as.setCustomNameVisible(true);
                }
                as.setHeadPose(decodeEulerAngle(s, "head"));
                as.setBodyPose(decodeEulerAngle(s, "body"));
                if (as.hasArms()) {
                    as.setLeftArmPose(decodeEulerAngle(s, "larm"));
                    as.setRightArmPose(decodeEulerAngle(s, "rarm"));
                }
                as.setLeftLegPose(decodeEulerAngle(s, "lleg"));
                as.setRightLegPose(decodeEulerAngle(s, "rleg"));
                as.setHelmet(s.getItemStack("helmet"));
                as.setChestplate(s.getItemStack("chest"));
                as.setLeggings(s.getItemStack("legs"));
                as.setBoots(s.getItemStack("boots"));
                as.setItemInHand(s.getItemStack("hand"));
            });
        } catch (RuntimeException e) {
            plugin.getLogger().log(Level.WARNING, "Failed to spawn armor stand " + key, e);
            return null;
        }
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
            return;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please specify the file to load.");
            return;
        }
        if (!plugin.validateFileName(args[0])) {
            sender.sendMessage(ChatColor.RED + "That file name is not allowed.");
            return;
        }

        var file = new File(plugin.getDataFolder(), args[0] + ".yml");
        if (!file.exists()) {
            sender.sendMessage(ChatColor.RED + "The file \"" + file.getName() + "\" doesn't exist.");
            return;
        }
        var load = YamlConfiguration.loadConfiguration(file);
        var relativeTo = ((Player) sender).getLocation().getBlock().getLocation();
        int count = 0;
        int complete = 0;
        for (var key : load.getKeys(false)) {
            count++;
            if (decodeAndSpawnArmorStand(load, key, relativeTo) != null) {
                complete++;
            }
        }
        sender.sendMessage(ArmorStandCopyPlugin.MAIN_COLOUR + "Loaded " + count
                + " armor stands (" + (count - complete) + " failed) from \""
                + file.getName() + "\".");
    }

    @Override
    public List<String> getCompletions(String[] args) {
        if (args.length == 1) {
            return plugin.getFileCompletions(args[0]);
        }

        return Collections.emptyList();
    }
}
