package net.cubekrowd.armorstandcopy;

import java.util.List;
import org.bukkit.command.CommandSender;

public interface Subcommand {
    String getName();

    String getUsage();

    String getDescription();

    void execute(CommandSender sender, String[] args);

    default void execute(CommandSender sender) {
        execute(sender, new String[0]);
    }

    List<String> getCompletions(String[] args);
}
