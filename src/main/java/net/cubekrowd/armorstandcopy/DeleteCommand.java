package net.cubekrowd.armorstandcopy;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;

public final class DeleteCommand implements Subcommand {
    private final ArmorStandCopyPlugin plugin;

    public DeleteCommand(ArmorStandCopyPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "delete";
    }

    @Override
    public String getUsage() {
        return "<file>";
    }

    @Override
    public String getDescription() {
        return "Delete a saved armor stand file.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please specify the file to delete.");
            return;
        }
        if (!plugin.validateFileName(args[0])) {
            sender.sendMessage(ChatColor.RED + "That file name is not allowed.");
            return;
        }

        var file = new File(plugin.getDataFolder(), args[0] + ".yml");
        if (!file.exists()) {
            sender.sendMessage(ChatColor.RED + "The file \"" + file.getName() + "\" doesn't exist.");
            return;
        }

        if (!file.delete()) {
            sender.sendMessage(ChatColor.RED + "Something went wrong while deleting \"" + file.getName() + "\".");
        } else {
            sender.sendMessage(ArmorStandCopyPlugin.MAIN_COLOUR + "Deleted the file \"" + file.getName() + "\".");
        }
    }

    @Override
    public List<String> getCompletions(String[] args) {
        if (args.length == 1) {
            return plugin.getFileCompletions(args[0]);
        }

        return Collections.emptyList();
    }
}
