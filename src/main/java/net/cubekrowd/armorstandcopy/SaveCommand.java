package net.cubekrowd.armorstandcopy;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public final class SaveCommand implements Subcommand {
    private final ArmorStandCopyPlugin plugin;

    public SaveCommand(ArmorStandCopyPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "save";
    }

    @Override
    public String getUsage() {
        return "<radius> <file>";
    }

    @Override
    public String getDescription() {
        return "Saves the armor stands within the radius around you to the file.";
    }

    private void encodeVector(ConfigurationSection section, String key, Vector vec) {
        var s = section.createSection(key);
        s.set("X", vec.getX());
        s.set("Y", vec.getY());
        s.set("Z", vec.getZ());
    }

    private void encodeEulerAngle(ConfigurationSection section, String key, EulerAngle angle) {
        var s = section.createSection(key);
        s.set("X", angle.getX());
        s.set("Y", angle.getY());
        s.set("Z", angle.getZ());
    }

    private void encodeArmorStand(ConfigurationSection section, String key, Location relativeTo,
            ArmorStand as) {
        var s = section.createSection(key);
        encodeEulerAngle(s, "body", as.getBodyPose());
        encodeEulerAngle(s, "head", as.getHeadPose());
        if (as.hasArms()) {
            encodeEulerAngle(s, "larm", as.getLeftArmPose());
            encodeEulerAngle(s, "rarm", as.getRightArmPose());
        }
        encodeEulerAngle(s, "lleg", as.getLeftLegPose());
        encodeEulerAngle(s, "rleg", as.getRightLegPose());
        s.set("helmet", as.getHelmet());
        s.set("chest", as.getChestplate());
        s.set("legs", as.getLeggings());
        s.set("boots", as.getBoots());
        s.set("hand", as.getItemInHand());
        encodeVector(s, "location", as.getLocation().subtract(relativeTo).toVector());
        encodeVector(s, "rotation", as.getLocation().getDirection());
        s.set("gravity", as.hasGravity());
        s.set("invuln", as.isInvulnerable());
        s.set("baseplate", as.hasBasePlate());
        s.set("usearms", as.hasArms());
        s.set("small", as.isSmall());
        s.set("vis", as.isVisible());
        if (as.isCustomNameVisible()) {
            s.set("display_name", as.getCustomName());
        }
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
            return;
        }
        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Please specify the radius to "
                    + "search and the target file.");
            return;
        }
        if (!plugin.validateFileName(args[1])) {
            sender.sendMessage(ChatColor.RED + "That file name is not allowed.");
            return;
        }

        var file = new File(plugin.getDataFolder(), args[1] + ".yml");

        if (file.exists()) {
            sender.sendMessage(ChatColor.RED + "That file already exists.");
            return;
        }

        if (!file.getAbsoluteFile().getParentFile().exists()) {
            file.getAbsoluteFile().getParentFile().mkdirs();
        }
        var save = YamlConfiguration.loadConfiguration(file);
        double distance;

        try {
            distance = Double.parseDouble(args[0]);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "That radius is invalid.");
            return;
        }

        var player = (Player) sender;
        var relativeTo = player.getLocation().getBlock().getLocation();
        int count = 0;
        for (Entity ent : player.getNearbyEntities(distance, distance, distance)) {
            if (ent instanceof ArmorStand) {
                encodeArmorStand(save, ent.getUniqueId().toString(), relativeTo, (ArmorStand) ent);
                count++;
            }
        }
        try {
            save.save(file);
            sender.sendMessage(ArmorStandCopyPlugin.MAIN_COLOUR + "Saved "
                    + count + " armor stands to \"" + file.getName() + "\".");
        } catch (IOException e) {
            sender.sendMessage(ChatColor.RED + "Failed to save file!");
        }
    }

    @Override
    public List<String> getCompletions(String[] args) {
        return Collections.emptyList();
    }
}
