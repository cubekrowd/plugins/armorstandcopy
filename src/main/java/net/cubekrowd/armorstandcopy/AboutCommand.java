package net.cubekrowd.armorstandcopy;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class AboutCommand implements Subcommand {
    private final ArmorStandCopyPlugin plugin;

    public AboutCommand(ArmorStandCopyPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Displays plugin info.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        var authors = new StringJoiner(", ");
        plugin.getDescription().getAuthors().forEach(authors::add);

        var joiner = new StringJoiner("\n");
        joiner.add(ChatColor.GRAY + "[" + ArmorStandCopyPlugin.MAIN_COLOUR + plugin.getName() + ChatColor.GRAY + "]");
        joiner.add(ChatColor.WHITE + "Authors: " + authors);
        joiner.add(ChatColor.WHITE + "Version: " + plugin.getDescription().getVersion());
        joiner.add(ChatColor.WHITE + "Website: " + plugin.getDescription().getWebsite());

        sender.sendMessage(joiner.toString());
    }

    @Override
    public List<String> getCompletions(String[] args) {
        return Collections.emptyList();
    }
}
