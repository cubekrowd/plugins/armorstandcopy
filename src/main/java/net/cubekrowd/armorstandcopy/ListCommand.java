package net.cubekrowd.armorstandcopy;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import org.bukkit.command.CommandSender;

public final class ListCommand implements Subcommand {
    private final ArmorStandCopyPlugin plugin;

    public ListCommand(ArmorStandCopyPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "list";
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Shows the list of saved files.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        var fileNames = plugin.getDataFolder().list();

        if (fileNames == null || fileNames.length == 0) {
            sender.sendMessage(ArmorStandCopyPlugin.MAIN_COLOUR + "There are "
                    + "no saved armor stand files currently.");
            return;
        }

        var joiner = new StringJoiner(", ");

        for (var fileName : fileNames) {
            if (fileName.endsWith(".yml")) {
                // trim extension
                joiner.add(fileName.substring(0, fileName.length() - 4));
            }
        }

        sender.sendMessage(ArmorStandCopyPlugin.MAIN_COLOUR
                + "Armor stand files: " + joiner.toString() + ".");
    }

    @Override
    public List<String> getCompletions(String[] args) {
        return Collections.emptyList();
    }
}
