package net.cubekrowd.armorstandcopy;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;

public final class HelpCommand implements Subcommand {
    private final ArmorStandCopyPlugin plugin;

    public HelpCommand(ArmorStandCopyPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display the help page.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        var joiner = new StringJoiner("\n");
        joiner.add(ChatColor.GRAY + "[" + ArmorStandCopyPlugin.MAIN_COLOUR + plugin.getName() + ChatColor.GRAY + "]");

        for (var cmd : plugin.getSubcommands()) {
            var usage = cmd.getUsage() == null ? "" : " " + cmd.getUsage();

            joiner.add(ChatColor.GRAY + "- /asc " + cmd.getName() + usage
                    + ChatColor.WHITE + " - " + cmd.getDescription());
        }

        sender.sendMessage(joiner.toString());
    }

    @Override
    public List<String> getCompletions(String[] args) {
        return Collections.emptyList();
    }
}
