package net.cubekrowd.armorstandcopy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public final class ArmorStandCopyPlugin extends JavaPlugin {
    public static final ChatColor MAIN_COLOUR = ChatColor.BLUE;
    private HelpCommand helpCommand;
    private List<Subcommand> subcommands;

    public List<Subcommand> getSubcommands() {
        return subcommands;
    }

    public boolean validateFileName(String fileName) {
        // Ensure file names don't contain characters with special meanings
        // such as "..", so players can't mess around with the file system.
        var cpIter = fileName.codePoints().iterator();

        while (cpIter.hasNext()) {
            int cp = cpIter.nextInt();
            if (0x61 <= cp && cp <= 0x7a) { // a - z
                continue;
            }
            if (0x41 <= cp && cp <= 0x5a) { // A - Z
                continue;
            }
            if (0x30 <= cp && cp <= 0x39) { // 0 - 9
                continue;
            }
            if (cp == 0x2d || cp == 0x5f) { // -, _
                continue;
            }

            // Illegal character if we end up here.
            return false;
        }

        return true;
    }

    public List<String> getFileCompletions(String partialFileName) {
        var fileNames = getDataFolder().list();

        if (fileNames == null || fileNames.length == 0) {
            return Collections.emptyList();
        }

        var prefix = partialFileName.toLowerCase(Locale.ENGLISH);
        var result = new ArrayList<String>(fileNames.length);

        for (var fileName : fileNames) {
            if (fileName.endsWith(".yml")
                    && fileName.toLowerCase(Locale.ENGLISH).startsWith(prefix)) {
                result.add(fileName.substring(0, fileName.length() - 4));
            }
        }

        // sort with the natural order of strings, i.e. lexicographical order
        result.sort(null);
        return result;
    }

    @Override
    public void onEnable() {
        helpCommand = new HelpCommand(this);
        subcommands = List.of(
                helpCommand,
                new SaveCommand(this),
                new LoadCommand(this),
                new DeleteCommand(this),
                new ListCommand(this),
                new AboutCommand(this));
    }

    @Override
    public void onDisable() {
        helpCommand = null;
        subcommands = null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("armorstand.copy.admin")) {
            sender.sendMessage(ChatColor.RED + "You are not allowed to use this command.");
            return true;
        }
        if (args.length == 0) {
            helpCommand.execute(sender);
        } else {
            var arg = args[0].toLowerCase(Locale.ENGLISH);
            Subcommand targetCmd = helpCommand;

            for (var cmd : subcommands) {
                if (cmd.getName().equals(arg)) {
                    targetCmd = cmd;
                    break;
                }
            }

            targetCmd.execute(sender, Arrays.copyOfRange(args, 1, args.length));
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command,
            String alias, String[] args) {
        if (!sender.hasPermission("armorstand.copy.admin")) {
            return Collections.emptyList();
        }

        // Note: number of arguments is always at least 1
        var arg = args[0].toLowerCase(Locale.ENGLISH);

        if (args.length == 1) {
            var result = new ArrayList<String>();

            for (var cmd : subcommands) {
                if (cmd.getName().startsWith(arg)) {
                    result.add(cmd.getName());
                }
            }
            return result;
        } else {
            for (var cmd : subcommands) {
                if (cmd.getName().equals(arg)) {
                    return cmd.getCompletions(Arrays.copyOfRange(args, 1, args.length));
                }
            }
            return Collections.emptyList();
        }
    }
}
